<main id="main">

	<!-- ======= About Us Section ======= -->
	<section id="about" class="about">
		<div class="container" data-aos="fade-up">

			<div class="section-header">
				<h2>Tujuan Proyek</h2>
				<p>"Proyek ini bertujuan untuk meningkatkan kualitas tata kelola penyelengaraan pendidikan dasar dan menengah di Kementerian Agama (Kemenag).
					Proyek ini akan dilaksanakan dalam waktu lima tahun, dimulai dengan pelaksanaan proyek pada tahun 2020 dan berakhir pada tahun 2024.
					Proyek ini akan dilaksanakan di seluruh 34 provinsi dan 514 kabupaten/kota.</p>
			</div>

			<div class="row gy-4">
				<div class="col-lg-6">
					<h3>Madrasah Education Quality Reform </h3>
					<!-- <img src="https://madrasahreform.kemenag.go.id/__statics/img/logocolor.png" class="img-fluid rounded-4 mb-4" alt=""> -->
					<p>Komponen ini bertujuan untuk meningkatkan kapasitas madrasah dan satuan pendidikan penerima BOS di bawah binaan Kemenag untuk dapat mengembangkan, menyimpan, dan mengelola rencana kegiatan dan anggaran madrasah, terutama yang bersumber dari dana BOS, serta memantau pelaksanaan kegiatan dan anggaran madrasah.</p>
					<p>Selain itu, komponen ini juga bertujuan untuk memberikan dukungan dana bantuan kinerja dan afirmasi bagi madrasah untuk mempercepat pencapaian SNP. Pada awal tahun pelajaran, proses perencanaan dan penganggaran madrasah dimulai dengan pelaksanaan EDM yang bertujuan untuk mengukur ketercapaian delapan SNP. EDM ini akan menghasilkan skor yang dicapai untuk 8 SNP.</p>
					<p>Melalui proses ini, madrasah dapat mengidentifikasi standar mana yang telah dicapai dan standar mana yang masih perlu ditingkatkan, sehingga hal ini akan membantu madrasah dalam menentukan standar mana yang perlu mendapatkan prioritas untuk ditingkatkan.</p>
				</div>
				<div class="col-lg-6">
					<div class="content ps-0 ps-lg-5">
						<!-- <p class="fst-italic">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua.
						</p>
						<ul>
							<li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
							<li><i class="bi bi-check-circle-fill"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
							<li><i class="bi bi-check-circle-fill"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
						</ul> -->
						<p>
							Madrasah akan menggunakan informasi tersebut untuk mengidentifikasi target-target yang akan dicapai pada akhir tahun pelajaran dan dalam empat tahun. Target tersebut juga digunakan madrasah dalam mengembangkan rencana kegiatan dan anggaran madrasah setiap tahun dengan target untuk meningkatkan capaian SNP. Madrasah juga akan membuat “Perjanjian Kinerja” dengan Kepala Kantor Kemenag Kabupaten/Kota tentang target yang akan dicapai, di mana pencapaian standar tersebut akan dievaluasi setiap tahunnya.
						</p>

						<div class="position-relative mt-4">
							<img src="https://madrasahreform.kemenag.go.id/__statics/img/logocolor.png" class="img-fluid rounded-4" alt="">
							<a href="https://www.youtube.com/watch?v=alaxp8Brm_E" class="glightbox play-btn"></a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section><!-- End About Us Section -->

	<!-- ======= Recent Blog Posts Section ======= -->
	<section id="recent-posts" class="recent-posts sections-bg">
		<div class="container" data-aos="fade-up">

			<div class="section-header">
				<h2>Berita Terkini</h2>
				<p>Dokuentasi dan pemberitaan tentang proyek</p>
			</div>

			<div class="row gy-4">

				<div class="col-xl-3 col-md-6">
					<article>

						<div class="post-img">
							<img src="assets/img/blog/blog-1.jpg" alt="" class="img-fluid">
						</div>

						<p class="post-category">Politics</p>

						<h2 class="title">
							<a href="blog-details.html">Dolorum optio tempore voluptas dignissimos</a>
						</h2>

						<div class="d-flex align-items-center">
							<img src="assets/img/blog/blog-author.jpg" alt="" class="img-fluid post-author-img flex-shrink-0">
							<div class="post-meta">
								<p class="post-author">Maria Doe</p>
								<p class="post-date">
									<time datetime="2022-01-01">Jan 1, 2022</time>
								</p>
							</div>
						</div>

					</article>
				</div><!-- End post list item -->

				<div class="col-xl-3 col-md-6">
					<article>

						<div class="post-img">
							<img src="assets/img/blog/blog-1.jpg" alt="" class="img-fluid">
						</div>

						<p class="post-category">Politics</p>

						<h2 class="title">
							<a href="blog-details.html">Dolorum optio tempore voluptas dignissimos</a>
						</h2>

						<div class="d-flex align-items-center">
							<img src="assets/img/blog/blog-author.jpg" alt="" class="img-fluid post-author-img flex-shrink-0">
							<div class="post-meta">
								<p class="post-author">Maria Doe</p>
								<p class="post-date">
									<time datetime="2022-01-01">Jan 1, 2022</time>
								</p>
							</div>
						</div>

					</article>
				</div><!-- End post list item -->

				<div class="col-xl-3 col-md-6">
					<article>

						<div class="post-img">
							<img src="assets/img/blog/blog-2.jpg" alt="" class="img-fluid">
						</div>

						<p class="post-category">Sports</p>

						<h2 class="title">
							<a href="blog-details.html">Nisi magni odit consequatur autem nulla dolorem</a>
						</h2>

						<div class="d-flex align-items-center">
							<img src="assets/img/blog/blog-author-2.jpg" alt="" class="img-fluid post-author-img flex-shrink-0">
							<div class="post-meta">
								<p class="post-author">Allisa Mayer</p>
								<p class="post-date">
									<time datetime="2022-01-01">Jun 5, 2022</time>
								</p>
							</div>
						</div>

					</article>
				</div><!-- End post list item -->

				<div class="col-xl-3 col-md-6">
					<article>

						<div class="post-img">
							<img src="assets/img/blog/blog-3.jpg" alt="" class="img-fluid">
						</div>

						<p class="post-category">Entertainment</p>

						<h2 class="title">
							<a href="blog-details.html">Possimus soluta ut id suscipit ea ut in quo quia et soluta</a>
						</h2>

						<div class="d-flex align-items-center">
							<img src="assets/img/blog/blog-author-3.jpg" alt="" class="img-fluid post-author-img flex-shrink-0">
							<div class="post-meta">
								<p class="post-author">Mark Dower</p>
								<p class="post-date">
									<time datetime="2022-01-01">Jun 22, 2022</time>
								</p>
							</div>
						</div>

					</article>
				</div><!-- End post list item -->

			</div><!-- End recent posts list -->

		</div>
	</section><!-- End Recent Blog Posts Section -->

	<!-- ======= Frequently Asked Questions Section ======= -->
	<section id="faq" class="faq">
		<div class="container" data-aos="fade-up">

			<div class="row gy-4">

				<div class="col-lg-4">
					<div class="content px-xl-5">
						<h3><strong>Pertanyaan</strong> Yang Sring Diajukan</h3>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
						</p>
					</div>
				</div>

				<div class="col-lg-8">

					<div class="accordion accordion-flush" id="faqlist" data-aos="fade-up" data-aos-delay="100">

						<div class="accordion-item">
							<h3 class="accordion-header">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
									<span class="num">1.</span>
									Apa itu proyek MEQR?
								</button>
							</h3>
							<div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist">
								<div class="accordion-body">
									Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
								</div>
							</div>
						</div><!-- # Faq item-->

						<div class="accordion-item">
							<h3 class="accordion-header">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2">
									<span class="num">2.</span>
									Berapa Lama Proyek berlangsung?
								</button>
							</h3>
							<div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist">
								<div class="accordion-body">
									Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
								</div>
							</div>
						</div><!-- # Faq item-->

						<div class="accordion-item">
							<h3 class="accordion-header">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3">
									<span class="num">3.</span>
									Siapa PIC proyek MEQR?
								</button>
							</h3>
							<div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist">
								<div class="accordion-body">
									Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
								</div>
							</div>
						</div><!-- # Faq item-->

						<div class="accordion-item">
							<h3 class="accordion-header">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-4">
									<span class="num">4.</span>
									Sasaran dari proyek MEQR?
								</button>
							</h3>
							<div id="faq-content-4" class="accordion-collapse collapse" data-bs-parent="#faqlist">
								<div class="accordion-body">
									Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
								</div>
							</div>
						</div><!-- # Faq item-->
					</div>

				</div>
			</div>

		</div>
	</section><!-- End Frequently Asked Questions Section -->

	<!-- ======= Clients Section ======= -->
	<section id="clients" class="clients">
		<div class="container" data-aos="zoom-out">

			<div class="clients-slider swiper">
				<div class="swiper-wrapper align-items-center">
					<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/erkam_warna.png" class="img-fluid" alt=""></div>
					<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/EMIS.png" class="img-fluid" alt=""></div>
					<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/mc.png" class="img-fluid" alt=""></div>
					<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/mrc.png" class="img-fluid" alt=""></div>
				</div>
			</div>

		</div>
	</section><!-- End Clients Section -->

</main><!-- End #main -->