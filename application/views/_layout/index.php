<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Madrasah Reform</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Favicons -->
	<link href="assets/img/favicon.png" rel="icon">
	<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

	<!-- Google Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="assets/vendor/aos/aos.css" rel="stylesheet">
	<link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
	<link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

	<!-- Template Main CSS File -->
	<link href="assets/css/main.css" rel="stylesheet">

	<!-- =======================================================
  * Template Name: Impact - v1.0.0
  * Template URL: https://bootstrapmade.com/impact-bootstrap-business-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

	<!-- ======= Header ======= -->
	<section id="topbar" class="topbar d-flex align-items-center">
		<div class="container d-flex justify-content-center justify-content-md-between">
			<div class="contact-info d-flex align-items-center">
				<i class="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@example.com">contact@example.com</a></i>
				<i class="bi bi-phone d-flex align-items-center ms-4"><span>+1 5589 55488 55</span></i>
			</div>
			<div class="social-links d-none d-md-flex align-items-center">
				<a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
				<a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
				<a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
				<a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
			</div>
		</div>
	</section><!-- End Top Bar -->

	<header id="header" class="header d-flex align-items-center">

		<div class="container-fluid container-xl d-flex align-items-center justify-content-between">
			<a href="index.html" class="logo d-flex align-items-center">
				<!-- Uncomment the line below if you also wish to use an image logo -->
				<!-- <img src="assets/img/logo.png" alt=""> -->
				<!-- <h1>Impact<span>.</span></h1> -->
				<img src="https://madrasahreform.kemenag.go.id/__statics/img/logowhite.png" />
			</a>
			<nav id="navbar" class="navbar">
				<ul>
					<li><a href="#hero">Beranda</a></li>
					<li class="dropdown"><a href="#about"><span>Tentang</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
						<ul>
							<li><a href="#">Komponen 1</a></li>
							<li><a href="#">Komponen 2</a></li>
							<li><a href="#">Komponen 3</a></li>
							<li><a href="#">Komponen 4</a></li>
						</ul>
					</li>
					<!-- <li><a href="#services">Berita</a></li> -->
					<li><a href="#portfolio">Sasaran Manfaat</a></li>
					<!-- <li><a href="#team">Team</a></li> -->
					<!-- <li><a href="blog.html">Berita</a></li> -->
					<li class="dropdown"><a href="#"><span>Berita</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
						<ul>
							<li><a href="#">Drop Down 1</a></li>
							<li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
								<ul>
									<li><a href="#">Deep Drop Down 1</a></li>
									<li><a href="#">Deep Drop Down 2</a></li>
									<li><a href="#">Deep Drop Down 3</a></li>
									<li><a href="#">Deep Drop Down 4</a></li>
									<li><a href="#">Deep Drop Down 5</a></li>
								</ul>
							</li>
							<li><a href="#">Drop Down 2</a></li>
							<li><a href="#">Drop Down 3</a></li>
							<li><a href="#">Drop Down 4</a></li>
						</ul>
					</li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</nav><!-- .navbar -->

			<i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
			<i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

		</div>
	</header><!-- End Header -->
	<!-- End Header -->



	<!-- ======= Hero Section ======= -->
	<section id="hero" class="hero">
		<div class="position-relative">

			<!-- slider -->
			<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="margin-top:-110px; background-color: #000;">
				<div class="carousel-indicators">
					<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
					<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
				</div>
				<div class="carousel-inner" style=" width:100%; height: 500px !important;">
					<div class="carousel-item active" style="opacity: 0.4; background-image: url('https://madrasahreform.kemenag.go.id/__statics/img/content/banner/gtkmadrasah_s_BrqNtMABn8R.jpg')">

					</div>
					<div class="carousel-caption">
						<h5>Realizing Education's Promise</h5>
						<h3>Madrasah Reform</h3>
						<p><b>Madrasah Education Quality Reform (IBRD 8992-ID)</b></p>
					</div>
					<div class="carousel-item" style="opacity: 0.5; background-image: url('https://madrasahreform.kemenag.go.id/__statics/img/content/banner/gtkmadrasah_s_BrqNtMCh0z5.jpg')">
						<div class="carousel-caption">
							<h5>Second slide label</h5>
							<p>Some representative placeholder content for the second slide.</p>
						</div>
					</div>
					<div class="carousel-item" style="opacity: 0.4; background-image: url('https://madrasahreform.kemenag.go.id/__statics/img/content/banner/PendidikanMadrasah_Bs6xjz3gZfL.jpg')">
						<div class="carousel-caption">
							<h5>Third slide label</h5>
							<p>Some representative placeholder content for the third slide.</p>
						</div>
					</div>
				</div>
				<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
			</div>
			<!-- end slider -->

		</div>

		<div class="icon-boxes position-relative">
			<div class="container position-relative">
				<div class="row gy-4 mt-5">

					<div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
						<div class="icon-box">
							<div class="icon"><i class="bi bi-easel"></i></div>
							<h4 class="title"><a href="" class="stretched-link">Komponen 1</a></h4>
						</div>
					</div>
					<!--End Icon Box -->

					<div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
						<div class="icon-box">
							<div class="icon"><i class="bi bi-people"></i></div>
							<h4 class="title"><a href="" class="stretched-link">Komponen 2</a></h4>
						</div>
					</div>
					<!--End Icon Box -->

					<div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
						<div class="icon-box">
							<div class="icon"><i class="bi bi-person-video3"></i></div>
							<h4 class="title"><a href="" class="stretched-link">Komponen 3</a></h4>
						</div>
					</div>
					<!--End Icon Box -->

					<div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="500">
						<div class="icon-box">
							<div class="icon"><i class="bi bi-puzzle"></i></div>
							<h4 class="title"><a href="" class="stretched-link">Komponen 4</a></h4>
						</div>
					</div>
					<!--End Icon Box -->

				</div>
			</div>
		</div>

		</div>
	</section>
	<!-- End Hero Section -->

	<main id="main">
		<?php
		$this->load->view($content);
		?>
	</main><!-- End #main -->

	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">

		<div class="container">
			<div class="row gy-4">
				<div class="col-lg-5 col-md-12 footer-info">
					<a href="index.html" class="logo d-flex align-items-center">
						<span>Madrasah Reform</span>
					</a>
					<p>Madrasah Education Quality Reform (IBRD 8992-ID).</p>
					<div class="social-links d-flex mt-4">
						<a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
						<a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
						<a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
						<a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
					</div>
				</div>

				<div class="col-lg-2 col-6 footer-links">
					<h4>Useful Links</h4>
					<ul>
						<li><a href="#">Beranda</a></li>
						<li><a href="#">Tentang</a></li>
						<li><a href="#">Sasaran Manfaat</a></li>
						<li><a href="#">Berita</a></li>
						<!-- <li><a href="#">Kontak</a></li> -->
					</ul>
				</div>

				<!-- <div class="col-lg-2 col-6 footer-links">
					<h4>Our Services</h4>
					<ul>
						<li><a href="#">Web Design</a></li>
						<li><a href="#">Web Development</a></li>
						<li><a href="#">Product Management</a></li>
						<li><a href="#">Marketing</a></li>
						<li><a href="#">Graphic Design</a></li>
					</ul>
				</div> -->

				<div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
					<h4>Kontak Kami</h4>
					<p>
						A108 Adam Street <br>
						New York, NY 535022<br>
						United States <br><br>
						<strong>Phone:</strong> +1 5589 55488 55<br>
						<strong>Email:</strong> info@example.com<br>
					</p>

				</div>

			</div>
		</div>

		<div class="container mt-4">
			<div class="copyright">
				&copy; Copyright <strong><span>Impact</span></strong>. All Rights Reserved
			</div>
			<div class="credits">
				<!-- All the links in the footer should remain intact. -->
				<!-- You can delete the links only if you purchased the pro version. -->
				<!-- Licensing information: https://bootstrapmade.com/license/ -->
				<!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/impact-bootstrap-business-website-template/ -->
				Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
			</div>
		</div>

	</footer><!-- End Footer -->
	<!-- End Footer -->

	<a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<div id="preloader"></div>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>
	<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
	<script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
	<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>

</body>

</html>